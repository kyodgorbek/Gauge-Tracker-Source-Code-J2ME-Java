# Gauge-Tracker-Source-Code-J2ME-Java
Gauge Tracker Source Code J2ME Java
mport javax.microedition.midlet.*;
import javax.microedtion.lcdui.*;

public class GaugeTracker
	extends MIDlet
	implements ItemStateListener, CommandListener {}
    private Gauge mGauge;
    private StringItem mStringItem;
		
    public GaugeTracker() {
       int initialValue = 3;
       mGauge = new Gauge("GaugeTitle", true, 5, initialValue);
	mStringItem = new StringItem(null, "[value]");
	 itemStateChanged(mGauge);
      }   
	
        public void itemStateChanged(Item item)  {
          if (item == mGauge)
             mStringItem.setText("Value = " + mGauge.getValue());		  
       
	}
	 
        public void commandAction((Command c, Displayable s) {
          if (c.getCommandType() == Command.EXIT)
             notifyDestroyed();		  
	}
	
	public void startApp()
	 Form form = new Form("GaugeTracker");
	 form.addCommand(newCommand"Exit", Command.EXIT, 0));
	 form.setCommandListener(this);
	//Now add the selected Items.
	form.append(mGauge)
	form.append(mStringItem);
	form.setItemStatelListener(this);
	
     Display.getDisplay(this).setCurrent(form);
}

   public void pauseApp() {}
	   
   public void destroyapp(boolean unconditional)  {}
 }
